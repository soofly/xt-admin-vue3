export default {
  route: {
    login: 'Login',
    personal: 'Personal Setting',
    dashboard: 'Dashboard'
  },
  tabbar: {
    refresh: 'refresh',
    delete: 'close',
    deleteLeft: 'close left',
    deleteRight: 'close right',
    deleteOther: 'close other'
  },
  login: {
    title: 'User Login',
    account: 'Account',
    password: 'Password',
    loginBtn: 'Login',
    accountRule: 'Account is required',
    passwordRule: 'Password cannot be less than 6 digits'
  },
  personal: {
    personal: 'Personal Setting',
    loginOut: 'Login Out'
  },
  notfound: {
    title: 'Page not found',
    desc: 'The page you are looking for does not exist',
    back: 'HOME'
  },
  theme: {
    themeChange: 'Theme Change',
    logo: 'Logo',
    mianNav: 'Mian Nav',
    subNav: 'Sub Nav',
    other: 'Other',
    mianContentBgColor: 'Main Bg Color',
    logoBgColor: 'Bg Color',
    logoTextColor: 'Text Color',
    menuBgColor: 'Bg Color',
    menuActiveBgColor: 'Active Bg Color',
    menuTextColor: 'Text Color',
    menuActiveTextColor: 'Active Text Color'
  },
  btn: {
    confirm: 'confirm',
    cancel: 'cancel'
  }
}
