export default {
  route: {
    login: '登录',
    dashboard: '首页',
    personal: '个人设置'
  },
  tabbar: {
    refresh: '刷新',
    delete: '关闭',
    deleteLeft: '关闭左侧',
    deleteRight: '关闭右侧',
    deleteOther: '关闭其他'
  },
  login: {
    title: '用户登录',
    account: '账号',
    password: '密码',
    loginBtn: '登录',
    accountRule: '用户名为必填项',
    passwordRule: '密码不能少于6位'
  },
  personal: {
    personal: '个人设置',
    loginOut: '退出'
  },
  notfound: {
    title: '未找到页面',
    desc: '您正在寻找的页面不存在',
    back: '首页'
  },
  theme: {
    themeChange: '主题更换',
    logo: 'Logo',
    mianNav: '主导航',
    subNav: '侧导航',
    other: '其他',
    mianContentBgColor: '主区域背景色',
    logoBgColor: '背景色',
    logoTextColor: '文字颜色',
    menuBgColor: '背景色',
    menuActiveBgColor: '选中背景色',
    menuTextColor: '文字颜色',
    menuActiveTextColor: '选中文字颜色'
  },
  btn: {
    confirm: '确定',
    cancel: '取消'
  }
}
