export default {
  route: {
    login: '登錄',
    personal: '個人設置',
    dashboard: '首頁'
  },
  tabbar: {
    refresh: '刷新',
    delete: '關閉',
    deleteLeft: '關閉左側',
    deleteRight: '關閉右側',
    deleteOther: '關閉其他'
  },
  login: {
    title: '用戶登錄',
    account: '賬號',
    password: '密碼',
    loginBtn: '登錄',
    accountRule: '用戶名為必填項',
    passwordRule: '密碼不能少於6位'
  },
  personal: {
    personal: '個人設置',
    loginOut: '退出'
  },
  notfound: {
    title: '未找到頁面',
    desc: '您正在尋找的頁面不存在',
    back: '首頁'
  },
  theme: {
    themeChange: '主題更換',
    logo: 'Logo',
    mianNav: '主導航',
    subNav: '側導航',
    other: '其他',
    mianContentBgColor: '主區域背景色',
    logoBgColor: '背景色',
    logoTextColor: '文字顏色',
    menuBgColor: '背景色',
    menuActiveBgColor: '選中背景色',
    menuTextColor: '文字顏色',
    menuActiveTextColor: '選中文字顏色'
  },
  btn: {
    confirm: '確定',
    cancel: '取消'
  }
}
