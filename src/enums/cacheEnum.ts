// localStorage sessionStorage 前缀
// 处理同一域名下部署两套(及以上)系统 缓存值冲突问题
export const STORAGE_PREFIX = 'xt_'

// app设置缓存key
export const APP_CONFIG = 'app_config'
// tabbar设置缓存key
export const TABBAR = 'tabbar'
// user设置缓存key
export const USER = 'user'
